package client;

import model.Equation;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class Client {

    public static void main(String[] args) {
        String url = "http://localhost:8080/example";

        WebTarget target = ClientBuilder.newBuilder()
                .register(new LoggingFilter(true))
                .build().target(url);

        target.request()
              .post(Entity.entity(new Equation(1, 2), MediaType.APPLICATION_JSON));

        // target.request().get(); // GET Päringu näide
    }


}

